# our third paper
## something about this part   
1:This repository will upload the source code so you can directly run it in the platform,the source code is available [here](https://github.com/yumeng5/LOTClass),the corresponding paper is [**Text Classification Using Label Names Only: A Language Model Self-Training Approach**](https://arxiv.org/abs/2010.07245), published in EMNLP 2020.

2:we have uploaded the dataset of ag_news and imdb in current path,for our other experiment dataset, you can get the source [amazon](https://drive.google.com/file/d/1pRt5mPuuVbi-ZXD8QZzw_7DlAnEg3X15/view) and [dbpedia](https://drive.google.com/file/d/1nCQQAC6XwfnyKtzWlNElMtz4s12kxfe7/view),to download the dataset,I think may be you should get a vpn and an google account. 

3:the code will auto run the code nltk.download('stopwords'),we have provide you the ```stopwords.zip```and you should put it under your folder```nltk_data``` .

4:for the more about the running environment,please go to the path ```LOTClass-master/readme``` for detailed

## what should you known before this experiment
1: make sure you have at least one GPU on your device,you can directly run  [```LOTClass-master/agnews.sh```](agnews.sh), [```LOTClass-master/dbpedia.sh```](dbpedia.sh), [```LOTClass-master/imdb.sh```](imdb.sh) and [```LOTClassamazon.sh```](amazon.sh) if you have 2 GPUS with 10GB memory separately.while you can refer the [```readme.md```](readme.md) under the path```LOTClass-master/readme.md``` for detailed information.

2:unzip the dateset we provide for you in this current folder ```agnews_data.zip``` and ```imdb_data.zip``` then put them in the path ```\LOTClass-master\datasets``` with their accord folder ,you can operate as the dataset of amazon and dbpedia,to run your own dataset ,please refer```LOTClass-master/readme.md```  for valid message.

3.if your have run this model once and want to reproduce the result,please delete the ```out.txt```and ```/.pt```file in the ```LOTClass-master/datasets``` with the corresponding folder.else it will upload your last model to predict.
## result:

our test accuracy in the platform Gemini of ag_news reached 85.6% compared with the result 86.4% in the original paper! you can step into ```./result in ag_news ```
for the result.
please contact me with my email:196816647@qq.com if you have any questions.

thanks for everyone with ours' work in paper reproduction!!!
